package com.rpl.hd.teque;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegCompany extends AppCompatActivity {

    Button register;
    EditText username, password, conpass, email, compname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regcompany);

        register = (Button)findViewById(R.id.register);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        conpass = (EditText)findViewById(R.id.conpass);
        email = (EditText)findViewById(R.id.email);
        compname = (EditText)findViewById(R.id.companyname);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user = String.valueOf(username.getText());
                String pass = String.valueOf(password.getText());
                String confirm = String.valueOf(conpass.getText());
                String mail = String.valueOf(email.getText());
                String comp = String.valueOf(compname.getText());

                //validation start

                if (comp.equals("")&&user.equals("")&&pass.equals("")&&confirm.equals("")&&mail.equals("")){
                    Toast.makeText(getApplicationContext(), "Field cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else if (comp.equals("")){
                    Toast.makeText(getApplicationContext(), "Company Name cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else if (user.equals("")){
                    Toast.makeText(getApplicationContext(), "Username cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else if (mail.equals("")){
                    Toast.makeText(getApplicationContext(), "Email cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else if (pass.equals("")){
                    Toast.makeText(getApplicationContext(), "Password cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else if (confirm.equals("")){
                    Toast.makeText(getApplicationContext(), "You must write your confirmation password", Toast.LENGTH_SHORT).show();
                }
                else if (confirm != null){
                    if (confirm.equals(pass)){
                        finish();
                    }
                    else
                        Toast.makeText(getApplicationContext(), "Pasword not match", Toast.LENGTH_SHORT).show();
                }
                else
                    finish();
                //validation end
            }
        });

    }
}