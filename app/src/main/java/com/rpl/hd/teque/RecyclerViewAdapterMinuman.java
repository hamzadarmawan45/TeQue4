package com.rpl.hd.teque;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdapterMinuman extends RecyclerView.Adapter<RecyclerViewAdapterMinuman.ViewHolder> {

    Context context;
    List<GetDataAdapter> getDataAdapter;
//    ImageLoader imageLoader;

    public RecyclerViewAdapterMinuman(List<GetDataAdapter> getDataAdapter, Context context){
        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_minuman, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, final int position) {
        final GetDataAdapter getDataAdapter1 = getDataAdapter.get(position);

//        imageLoader = ServerImageParseAdapter.getInstance(context).getImageLoader();

//        imageLoader.get(getDataAdapter1.getImageServerUrl(),
//                ImageLoader.getImageListener(
//                        Viewholder.networkImageView,//Server Image
//                        R.mipmap.ic_launcher,//Before loading server image the default showing image.
//                        android.R.drawable.ic_dialog_alert //Error image if requested image dose not found on server.
//                )
//        );

        Viewholder.view.setBackgroundColor(getDataAdapter1.isSelected()? Color.YELLOW : Color.WHITE);
        Viewholder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataAdapter1.setSelected(!getDataAdapter1.isSelected());
                Viewholder.view.setBackgroundColor(getDataAdapter1.isSelected ? Color.YELLOW : Color.WHITE);
            }
        });

//        Viewholder.networkImageView.setImageUrl(getDataAdapter1.getImageServerUrl(), imageLoader);
        Viewholder.ImageTitleNameView.setText(getDataAdapter1.getImageTitleName());
        Viewholder.ImageDeskripsiName.setText(getDataAdapter1.getImageDeskripsiName());
    }

    @Override
    public int getItemCount() {
        return getDataAdapter == null ? 0 :getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView ImageTitleNameView;
        public TextView ImageDeskripsiName;
//        public NetworkImageView networkImageView ;
        public View view;
        public RelativeLayout check;

        public ViewHolder(View itemView) {
            super(itemView);
            ImageTitleNameView = (TextView) itemView.findViewById(R.id.textView_nama_minuman);
            ImageDeskripsiName = (TextView) itemView.findViewById(R.id.textView_harga_minuman) ;
//            networkImageView = (NetworkImageView) itemView.findViewById(R.id.VollyNetworkImageView2);
            check = (RelativeLayout) itemView.findViewById(R.id.card_relative_minuman);
            view = itemView;
        }
    }
}