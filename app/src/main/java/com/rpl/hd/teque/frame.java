package com.rpl.hd.teque;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by HD on 18/01/2017.
 */

public class frame extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame);

        getSupportFragmentManager().beginTransaction().add(R.id.container, new DaftarFragment()).commit();
    }
}
