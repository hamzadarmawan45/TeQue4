package com.rpl.hd.teque;

/**
 * Created by HD on 09/01/2017.
 */

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "http://teque.hol.es/backend/web/api/user/login";

    // Server user register url
    public static String URL_REGISTER = "http://teque.hol.es/backend/web/api/user/create";
}
