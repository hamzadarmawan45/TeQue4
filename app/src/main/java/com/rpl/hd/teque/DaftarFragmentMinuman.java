/*
 * Created by Nara Syai on 06/12/16 9:34
 * Copyright (c) 2016.  All right reserved.
 *
 * Last modified 11/09/15 9:40
 */

package com.rpl.hd.teque;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DaftarFragmentMinuman extends Fragment{
    List<GetDataAdapter> GetDataAdapter2;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;

    String GET_JSON_DATA_HTTP_URL = "http://teque.hol.es/backend/web/api/order/minuman?id=18"; //url json
    String JSON_NAME = "nama"; //json nama makana
    String JSON_HARGA = "harga"; //json harga
//    String JSON_IMAGE_URL = "http://teque.hol.es/backend/web/uploads/glass.png"; //json gambar

    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.fragment_daftar_minuman, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Daftar Minuman");

        GetDataAdapter2 = new ArrayList<>();
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycler_view_minuman);
        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(recyclerViewlayoutManager);

        JSON_DATA_WEB_CALL();
    }

    public void JSON_DATA_WEB_CALL(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait");
        progressDialog.show();

        jsonArrayRequest = new JsonArrayRequest(GET_JSON_DATA_HTTP_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.hide();
                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                        // Stop progress indicator when update finish
                        // mSwipeRefreshLayout.setRefreshing(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getActivity().getApplicationContext(), "Time Out Error", Toast.LENGTH_SHORT).show();
                        // Stop progress indicator when update finish
                        // mSwipeRefreshLayout.setRefreshing(false);
                    }
                });

        requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonArrayRequest);
    }

    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        for(int i = 0; i < array.length(); i++) {
            GetDataAdapter GetDataAdapter3 = new GetDataAdapter();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);
                GetDataAdapter3.setImageTitleNamee(json.getString(JSON_NAME));
                GetDataAdapter3.setImageDeskripsiNamee(json.getString(JSON_HARGA));
//                GetDataAdapter3.setImageServerUrl(json.getString(JSON_IMAGE_URL));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter2.add(GetDataAdapter3);
        }
        recyclerViewadapter = new RecyclerViewAdapterMinuman(GetDataAdapter2, getContext());
        recyclerView.setAdapter(recyclerViewadapter);
    }
}

