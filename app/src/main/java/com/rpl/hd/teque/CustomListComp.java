package com.rpl.hd.teque;

/**
 * Created by HD on 17/01/2017.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomListComp extends ArrayAdapter<String> {
    private String[] ids;
    private String[] names;
    private String[] emails;
    private Activity context;

    public CustomListComp (Activity context, String[] ids, String[] names, String[] emails) {
            super(context, R.layout.adaptor_list_comp, ids);
            this.context = context;
            this.ids = ids;
            this.names = names;
            this.emails = emails;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View listViewItem = inflater.inflate(R.layout.adaptor_list_comp, null, true);
            TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewId);
            TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
            TextView textViewEmail = (TextView) listViewItem.findViewById(R.id.textViewEmail);

            textViewId.setText(ids[position]);
            textViewName.setText(names[position]);
            textViewEmail.setText(emails[position]);

            return listViewItem;
        }
    }