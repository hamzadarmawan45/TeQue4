package com.rpl.hd.teque;

public class GetDataAdapter {
//    public String ImageServerUrl;
    public String ImageTitleName;
    public String ImageDeskripsiName;
    public boolean isSelected = false;

//    public String getImageServerUrl() {
//        return ImageServerUrl;
//    }

//    public void setImageServerUrl(String imageServerUrl) {
//        this.ImageServerUrl = imageServerUrl;
//    }

    public String getImageTitleName() {
        return ImageTitleName;
    }

    public void setImageTitleNamee(String Imagetitlename) {
        this.ImageTitleName = Imagetitlename;
    }

    public String getImageDeskripsiName() {
        return ImageDeskripsiName;
    }

    public void setImageDeskripsiNamee(String Imagedeskripsiname) {
        this.ImageDeskripsiName = Imagedeskripsiname;
    }

    public void setSelected(boolean selected){
        isSelected = selected;
    }

    public boolean isSelected(){
        return isSelected;
    }
}