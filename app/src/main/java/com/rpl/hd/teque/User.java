package com.rpl.hd.teque;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class User extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private static  int RESULT_LOAD_IMAGE = 1;

    private SQLiteHandler db;
    private SessionManager session;

    private TextView txtName;
    private Button order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtName = (TextView) findViewById(R.id.name);
        order = (Button) findViewById(R.id.btn_order);


        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();

        String name = user.get("username");

        // Displaying the user details on the screen
        txtName.setText(name);

        // list view
        ArrayList<String> list=new ArrayList<>();
        for (int i=0;i<40;i++) {
            list.add("Ritesh List Item "+i);
        }

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(User.this,LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(layoutManager);
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(User.this,list);
        recyclerView.setAdapter(adapter);

        //drawer initial start
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //drawer initial end

        ImageView loadImage = (ImageView) findViewById(R.id.img_prof);
        loadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(User.this, CompList.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //drawer content start
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.us_setting) {
            Intent intent = new Intent(User.this, Settings.class);
            startActivity(intent);
        } else if (id == R.id.us_logut) {
            logoutUser();
        } else if (id == R.id.us_about) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //drawer content end


    @Override
    protected void onActivityResult(int requestCode, int resultCOde, Intent data){
        super.onActivityResult(requestCode, resultCOde, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCOde == RESULT_OK && null != data){
            Uri selectedImage = data.getData();
            String[] filePathColoumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColoumn, null, null, null);
            cursor.moveToFirst();

            int coloumnIndex = cursor.getColumnIndex(filePathColoumn[0]);
            String picturePath = cursor.getString(coloumnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.img_prof);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(User.this, Login.class);
        startActivity(intent);
        finish();
    }

    //RecyclerView

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {
        private ArrayList<String> list;
        private Context context;
        private int lastPosition = -1;

        public MyRecyclerAdapter(Context context, ArrayList<String> list1) {
            this.list = list1;
            this.context = context;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_image, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder viewHolder, final int i) {

            viewHolder.textView_name.setText(String.valueOf(list.get(i)));


            viewHolder.lnCover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context,"Item Clicked "+i,Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            public RelativeLayout lnCover;
            public TextView textView_name;

            public ViewHolder(View view) {
                super(view);
                lnCover = (RelativeLayout) view
                        .findViewById(R.id.lnCover);
                textView_name = (TextView) view
                        .findViewById(R.id.textView_name);
            }
        }

    }


}